<?php

// If include function load a page then include_once
// function won't load the same page

// Note: But include function will load as many it can
// It may be same page or not

include "include_test.php";

include_once "include_test.php";