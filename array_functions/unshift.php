<?php

//array_unshift — Prepend one or more elements
// to the beginning of an array


$queue = array("orange", "banana");
array_unshift($queue, "apple", "raspberry");
print_r($queue);

echo "<hr>";

$a = [2,3,4];

array_unshift($a, 1,2,3,4,5,6, "ashiq", 'shaon');
print_r($a);

// 1,2,3,4,5,6, "ashiq", 'shaon', 2, 3, 4

//array_unshift() prepends passed elements to the front of the array.
// Note that the list of elements is prepended as a whole,
// so that the prepended elements stay in the same order.
// All numerical array keys will be modified to start counting
// from zero while literal keys won't be touched.