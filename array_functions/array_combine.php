<?php

//array_combine —
// Creates an array by using one array for
// keys and another for its values

$a = array(2,3,4);
$b = array("ashiq", 'shaon', 'nadim');

print_r(array_combine($a, $b));

// Note: array_combine():
// Both parameters should have an equal number of elements