<?php

// array_keys — Return all the keys or a subset of the keys of an array

echo "<pre>";
$array = array(0 => 100, "color" => "red");
print_r(array_keys($array));
echo "</pre>";
echo "<pre>";
$array = array("blue", "red", "green", "blue", "blue");
print_r(array_keys($array, "blue"));
echo "</pre>";
echo "<pre>";
$array = array("color" => array("blue", "red", "green"),
    "size"  => array("small", "medium", "large"));
print_r(array_keys($array));
echo "</pre>";

// Output

/*
 * Array
(
    [0] => 0
    [1] => color
)
Array
(
    [0] => 0
    [1] => 3
    [2] => 4
)
Array
(
    [0] => color
    [1] => size
)
 */

?>