<?php

// array_filter — Filters elements of an array
// using a callback function


function examine_modulus($var)
{

    return(($var % 2) == 0);
}



$array1 = array("a"=>2, "b"=>2, "c"=>3, "d"=>4, "e"=>5);


print_r(array_filter($array1, "examine_modulus"));
