<?php

//current — Return the current element in an array

//Every array has an internal pointer to its "current" element,
// which is initialized to the first element inserted into the array.

$a = [2,3,4,5];

echo current($a)."<br>"; // 2

echo next($a)."<br>"; // 3

echo current($a)."<br>"; // 3

echo end($a)."<br>"; // 5