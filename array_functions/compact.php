<?php


//compact — Create array containing variables and their values

$a = "ashiq";
$b = "nadim";

// here we need to pass the variable as string

$c = compact('a', 'b');

print_r($c);

// Array ( [a] => ashiq [b] => nadim )