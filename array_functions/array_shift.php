<?php

//array_shift — Shift an element off the beginning of array

$stack = array("orange", "banana", "apple", "raspberry");
$fruit = array_shift($stack);
print_r($stack);

/*
 * 
 * Array
(
    [0] => banana
    [1] => apple
    [2] => raspberry
)

 */