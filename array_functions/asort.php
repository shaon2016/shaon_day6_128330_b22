<?php

//asort — Sort an array and maintain index association

$a = [6,5,4,3,2];

asort($a);

print_r($a);

// Output
//Array ( [4] => 2 [3] => 3 [2] => 4 [1] => 5 [0] => 6 )