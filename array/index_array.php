<?php

// index array is only that has only index number or default array

$a[0] = "Shaon";
$a[1] = "Nadim";
$a[2] = "Ashiqul Islam";

foreach ($a as $key => $name)
    echo $key. " => " . $name. " <br>";

echo "<hr>";

//default array
$a = array('Shaon', 'Nadim', 'Ashiqul Islam');

foreach ($a as $key => $name)
    echo $key. " => " . $name. " <br>";

